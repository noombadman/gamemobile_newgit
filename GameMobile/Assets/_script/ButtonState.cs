﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonState : MonoBehaviour
{
    public enum State{ Up,Down}
    public  State _currentState;

   public void  SetDownState() 
   {
       _currentState = State.Down;
   }
     public void  SetUpState() 
   {
       _currentState = State.Up;
   }
}
