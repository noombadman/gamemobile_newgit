﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{
  public float totalTime;

  public  Text text;

  private  float minutes;
  private float seconds;
  private void Update()
  {
      totalTime -= Time.deltaTime;

      minutes = (int) (totalTime / 60  );
      seconds = (int) (  totalTime  % 60);

      text.text = minutes.ToString() +  "  :  "  +  seconds.ToString();
  }
}
